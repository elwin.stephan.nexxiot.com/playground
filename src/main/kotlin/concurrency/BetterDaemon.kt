package concurrency

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    val daemon = BetterDaemon()

    daemon.start()
    Thread.sleep(2000)
    daemon.stop()
}

/**
 * The purpose of this class is how the same thing as with [Daemon] can be achieved
 * by reading the documentation.
 */
class BetterDaemon {
    private lateinit var job: Job

    fun start() {
        job = CoroutineScope(Dispatchers.Default).launch {
            (0..100).forEach {
                println("Step $it")
                delay(1000)

                if (!isActive) {
                    return@launch
                }
            }
        }

        runBlocking {
            job.join()
        }
    }

    fun stop() {
        runBlocking {
            job.cancelAndJoin()
            println("1) Service has terminated by now")
        }

        println("2) Also, the stop function has terminated now (after the service)")
    }
}