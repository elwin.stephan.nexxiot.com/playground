# Concurrency & Flows

This project is a playground for some experimentation with coroutines and in particular with flows. Stuff here might be
wrong, use at your own risk.

## How to use this project

Head over to [`Coroutines.kt`](Coroutines.kt), you can run it and there are comments describing the different scenarios.


## Documentation
Kotlin has excellent docs with explanations:

- [Coroutines](https://kotlinlang.org/docs/coroutines-basics.html)
- [Suspending Functions](https://kotlinlang.org/docs/composing-suspending-functions.html#lazily-started-async)
- [Flows](https://kotlinlang.org/docs/flow.html)