
package concurrency

import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.buffer
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import java.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

val functions = listOf(
    ::longRunningServer,
//    ::sequential,
//    ::parallel,
//    ::sequenceBlocking,
//    ::flowConcurrent,
//    ::flowSequential,
//    ::flowPipelined,
//    ::fanout,
//    ::fanoutWithLimit,
//    ::fanoutWithLimitLibrary,
)

@OptIn(ExperimentalTime::class)
fun main() = runBlocking {
    functions.forEach { f ->
        println("Running ${f.name}")
        measureTime {
            f()
        }.let {
            println("Finished ${f.name} in ${it.inWholeMilliseconds}ms")
            println("------")
            println()
        }
    }
}

suspend fun longRunningServer() {
    coroutineScope {
        measure("fire-and-forget") {
            fireAndForget()
        }

        // Simulate long running server
        delay(10000)
    }
}

suspend fun fireAndForget() {
    coroutineScope {

        // This task simulates the request
        task("parent task, part 1", Duration.ofMillis(1000))

        // Job() makes sure that this coroutine runs independent
        // of the parent coroutineScope (which exits before the
        // forwarding call terminates).
        // W/o Job(), it is linked, and once the parent task exits
        // (and with it, the coroutineScope), TimeoutCancellationException
        // is thrown.
        launch {
            measure("forwarding block") {
                try {
                    withTimeout(5000) {
                        // This task simulates the forwarding
                        task("actual forwarding", Duration.ofMillis(6000))
                    }
                } catch (e: TimeoutCancellationException) {
                    println("Timeout exception thrown")
                }
            }
        }

        task("parent task, part 2", Duration.ofMillis(1000))
    }

    /**
     * Output with Job():
     *
     * Running caller
     * Starting fire-and-forget
     * Starting parent task, part 1
     * Finished parent task, part 1 in 1012ms
     * Starting parent task, part 1
     * Finished parent task, part 1 in 1008ms
     * Starting parent task, part 2
     * Starting forwarding block
     * Starting actual forwarding
     * Finished parent task, part 2 in 1005ms
     * Starting parent task, part 2
     * Finished parent task, part 2 in 1002ms
     * Finished fire-and-forget in 4055ms
     * Timeout exception thrown
     * Finished forwarding block in 5014ms
     * Finished caller in 14066ms
     * ------
     *
     * Output without Job():
     *
     * Running longRunningServer
     * Starting fire-and-forget
     * Starting parent task, part 1
     * Finished parent task, part 1 in 1022ms
     * Starting parent task, part 1
     * Finished parent task, part 1 in 1005ms
     * Starting parent task, part 2
     * Starting forwarding block
     * Starting actual forwarding
     * Finished parent task, part 2 in 1010ms
     * Starting parent task, part 2
     * Finished parent task, part 2 in 1005ms
     * Timeout exception thrown
     * Finished forwarding block in 5012ms
     * Finished fire-and-forget in 7068ms <- note this change
     * Finished longRunningServer in 17080ms
     * ------
     */
}

// This is a simple sequential computation with the tasks running back to back,
// taking 10s in total.
suspend fun sequential() {
    task("task-0")
    task("task-1")
}

// Here, the two computations are run in two separate coroutines (`launch`),
// taking 5s in total. `coroutineScope` waits for both routines to terminate.
suspend fun parallel() {
    coroutineScope {
        launch { task("task-0") }
        launch { task("task-1") }
    }
}

suspend fun sequenceBlocking() {
    val first = sequence {
        (0 until 5).onEach {
            runBlocking { task("0-concurrency.task-$it") } // Computations within a sequence are always blocking
            yield(it)

        }
    }

    first.last()
}

// As far as I can see, flow is equivalent to a sequence,
// except that computations in flows can be suspended,
// allowing e.g. multiple flows to run concurrently.
suspend fun flowConcurrent() {
    coroutineScope {
        val first = flow {
            (0 until 5).onEach {
                task("0-concurrency.task-$it") // Suspendable concurrency.getFunctions can only be called within a flow
                emit(it)
            }
        }

        val second = flow {
            (0 until 5).onEach {
                task("1-concurrency.task-$it")
                emit(it)
            }
        }

        println("Starting computation") // until here, no `ioIntensive` has been called
        launch { first.collect() }
        launch { second.collect() }
    }
}

// In this example, we can consider the computation to consist of
// three stages (0-, 1- and 2-concurrency.task-x). In a concurrency.sequential flow,
// once an item is requested (i.e. `collect`), the entire pipeline starts,
// i.e. each stage is walked through sequentially, taking 2,5s to fetch an item.
// Since the next item is only requested once the previous once has arrived,
// the total computation takes 5x 2.5s.
suspend fun flowSequential() {
    coroutineScope {
        flow {
            (0 until 5).onEach {
                task("0-concurrency.task-$it", Duration.ofMillis(500))
                emit(it)
            }
        }.onEach {
            task("1-concurrency.task-$it")
        }.onEach {
            task("2-concurrency.task-$it")
        }.collect()
    }
}

// Introducing a buffer basically does two things:
// - Call the remainder of the flow in a separate coroutine
// - Add a buffer (by default with size 64) and eagerly fill it
//   (i.e. before any item is requested)
// Thus, the computation is pipelined and takes
// - 5x 1s (the longest stage)
// - 0.5s + 1s (the remaining steps, once)
// - 6.5s in total
//
// Another sensible buffer size is Channel.RENDEZVOUS, conceptually
// a buffer with capacity 0. Here, items are produced only
// on demand (and not buffered). Backpressure is directly
// passed to the upstream producer.
// In this scenario, the overall duration of the computation
// remains the same. The effect of the buffer can be seen when
// removing the `collect()` at the end, as the pipeline will
// still fill up the buffers.
//
// Note that in any case, no two tasks from the same stage
// will ever be executed concurrently.
suspend fun flowPipelined() {
    coroutineScope {
        flow {
            (0 until 5).onEach {
                task("0-concurrency.task-$it", Duration.ofMillis(500))
                emit(it)
            }
        }.buffer().onEach {
            task("1-concurrency.task-$it")
        }.buffer().onEach {
            task("2-concurrency.task-$it")
        }.collect()
    }
}

// When tasks from a stage can be executed concurrently, the following pattern
// can be used: For each item, it creates an async job that is then awaited.
// Note that is possible that all tasks run concurrently, i.e. the following function
// will complete in about 1s, despite executing 50x 1s tasks.
suspend fun fanout() {
    coroutineScope {
        (0 until 50)
            .map {
                async { task("task-$it") }
            }.awaitAll()
    }

}

// Sometimes, executing too many tasks in parallel can lead to undesirable side effects
// such exhausting external resources or being blocked by rate-limits.
// The following pattern limits the number of outstanding requests,
// i.e. there may only be 10 tasks running concurrently at most.
// Depending on how fast the consumer is, this may result in
// batches of 10 tasks being run.
suspend fun fanoutWithLimit() {
    coroutineScope {
        (0 until 50).asFlow()
            .map {
                async { task("task-$it"); }
            }
            .buffer(10)
            .map { it.await() }
            .collect()
    }
}

// Of course, this logic can also be encapsulated into its own function:
suspend fun <T, R> Flow<T>.mapParallel(maxParallelism: Int = 10, transform: suspend (T) -> R): Flow<R> {
    val f = this

    return flow {
        coroutineScope {
            f
                .map { async { transform(it) } }
                .buffer(maxParallelism)
                .map { it.await() }
                .collect { emit(it) }
        }
    }
}

// Which results in this code with the equivalent functionality:
suspend fun fanoutWithLimitLibrary() {
    coroutineScope {
        (0 until 50).asFlow()
            .mapParallel(maxParallelism = 10) { task("task-$it") }
            .collect()
    }
}


@OptIn(ExperimentalTime::class)
suspend fun task(id: String, duration: Duration = Duration.ofSeconds(1)): Int {
    println("Starting $id")
    measureTime {
        delay(duration.toMillis())
    }.apply {
        println("Finished $id in ${this.inWholeMilliseconds}ms")
    }

    measure(id) {
        delay(duration.toMillis())
    }

    return 1
}

@OptIn(ExperimentalTime::class)
suspend fun measure(id: String, block: suspend () -> Unit) {
    println("Starting $id")
    measureTime {
        block()
    }.apply {
        println("Finished $id in ${this.inWholeMilliseconds}ms")
    }

    return
}