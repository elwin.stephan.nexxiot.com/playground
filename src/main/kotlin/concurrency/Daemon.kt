package concurrency

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    val daemon = Daemon()

    daemon.start()
    Thread.sleep(2000)
    daemon.stop()
}

/**
 * The purpose of this class is to demonstrate how a daemon (something with a non-blocking start
 * function that spawns a background-routine and a blocking stop that instructs the background
 * routine to terminate) can be created.
 */
class Daemon {
    private val chan = Channel<Int>()
    private val reply = Channel<Int>()

    fun start() {
        CoroutineScope(Dispatchers.Default).launch {
            (0..100).forEach {
                println(it)
                delay(1000)

                if (chan.tryReceive().isSuccess) {
                    reply.send(0)
                    return@launch
                }
            }
        }
    }

    fun stop() {
        // My understanding is that channels must be sent to and received from within coroutines.
        // They don't necessarily have to be started from the same coroutine context.
        runBlocking {
            chan.send(0)
            reply.receive()
            println("1) Service has terminated by now")
        }

        println("2) Also, the stop function has terminated now (after the service)")
    }
}